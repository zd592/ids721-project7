## Prerequisites

Before starting, ensure you have the following installed on your machine:
- Rust and Cargo (Follow the [official installation guide](https://www.rust-lang.org/tools/install))
- Docker (Refer to the [Docker installation guide](https://docs.docker.com/get-docker/))

## Building the Project

1. **Create the Project** (if starting from scratch):
   ```bash
   cargo new project7 --bin
   ```
   This command creates a new binary project named `project7`.

2. **Build the Project**:
   Navigate to the project directory:
   ```bash
   cd project7
   ```
   Then compile the project with:
   ```bash
   cargo build
   ```

## Running the Project

Start the project using Cargo:
```bash
cargo run
```
This command compiles (if necessary) and runs the `project7` application.

## Testing the Project

After the application is running, you can test it by sending a request to the server. Open a new terminal window and execute:
```bash
curl http://127.0.0.1:8080/
```
This command uses `curl` to send an HTTP GET request to the application. Adjust the URL as needed based on your application's routing and port settings.

## Dockerizing the Application

To containerize `project7`, follow these steps:

### 1. **Create a Dockerfile**

Create a `Dockerfile` in the root directory of your project with the following content:
```Dockerfile
# Build stage
FROM rust:1.67 as build
WORKDIR /usr/src/project7
COPY . .
RUN cargo build --release

# Runtime stage
FROM debian:buster-slim
COPY --from=build /usr/src/project7/target/release/project7 /usr/local/bin/project7
EXPOSE 8080
CMD ["project7"]
```

### 2. **Build the Docker Image**

From the root directory of your project, run:
```bash
docker build -t project7 .
```
This command builds a Docker image named `project7` from your Dockerfile.

### 3. **Run a Container**

Launch a container from your image:
```bash
docker run -d -p 8080:8080 --name project7_container project7
```
This runs your application in a container named `project7_container`, mapping port 8080 of the container to port 8080 on the host.

### 4. **Viewing Logs**

To check the logs of the running container, use:
```bash
docker logs project7_container
```

### 5. **Stopping and Removing the Container**

Stop the container with:
```bash
docker stop project7_container
```
And remove it with:
```bash
docker rm project7_container
```


# Data Processing with Vector Database

## Ensure that Qdrant is running and accessible at `http://localhost:6333`.

### Inserting Vectors

To insert vectors into Qdrant, use the following endpoint:

```bash
curl -X POST \
  http://localhost:8080/insert \
  -H 'Content-Type: application/json' \
  -d '{
    "id": 1,
    "vector": [0.1, 0.2, 0.3]
}'
```

Replace the vector data (`"id"` and `"vector"`) with your desired values.

### Searching Vectors

To search for vectors in Qdrant, use the following endpoint:

```bash
curl -X POST \
  http://localhost:8080/search \
  -H 'Content-Type: application/json' \
  -d '{
    "search_parameters": {
        // Include your search parameters here
    }
}'
```

Replace the search parameters with your desired values.

## Implementation Details

The project integrates Qdrant with Actix Web using Rust's `reqwest` crate for making HTTP requests to the Qdrant API. It defines two endpoints:

- `/insert`: Used for inserting vectors into Qdrant.
- `/search`: Used for searching vectors in Qdrant.

## Screen shots:
-  Test the function
    - ![Alt text](./images/0.jpeg "Optional title0")

-  Build the docker image
    - ![Alt text](./images/1.jpeg "Optional title1")

-  docker container
    - ![Alt text](./images/2.jpeg "Optional title2")

-  docker desktop
    - ![Alt text](./images/3.jpeg "Optional title3")

-  Vector Database
    - ![Alt text](./images/4.jpeg "Optional title3")

-  Vector Database terminal
    - ![Alt text](./images/5.jpeg "Optional title3")
