use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use reqwest::Client;

use serde::{Deserialize, Serialize};

// Define your structs here
#[derive(Serialize, Deserialize)]
struct Vector {
    id: u64,
    vector: Vec<f32>,
}

#[derive(Serialize, Deserialize)]
struct SearchResponse {
    result: Vec<Vector>,
}

async fn index() -> impl Responder {
    let client = Client::new();

    // Example of inserting a vector. Replace "your_vector" and "your_collection" accordingly.
    let insert_response = client.post("http://localhost:6333/collections/your_collection/points")
        .json(&Vector { id: 1, vector: vec![0.1, 0.2, 0.3] })
        .send()
        .await
        .expect("Failed to insert vector")
        .json::<serde_json::Value>()
        .await
        .expect("Failed to parse insert response");

    // Example of searching for similar vectors
    let search_response = client.post("http://localhost:6333/collections/your_collection/points/search")
        // Include your search parameters here
        .send()
        .await
        .expect("Failed to search vectors")
        .json::<SearchResponse>()
        .await
        .expect("Failed to parse search response");

    HttpResponse::Ok().json(search_response.result)
}

async fn search(search_parameters: web::Json<SearchParameters>) -> impl Responder {
    // Extract search parameters from the request body
    let params = search_parameters.into_inner();

    // Perform the search operation using Qdrant or other service
    // Replace this with your actual search logic
    let search_result = perform_search(params).await;

    // Return the search result as JSON
    HttpResponse::Ok().json(search_result)
}

#[derive(Serialize, Deserialize)]
struct SearchParameters {
    // Define your search parameters structure here
}

async fn perform_search(params: SearchParameters) -> SearchResponse {
    // Perform the actual search operation here
    // Replace this with your actual search logic
    let result = vec![Vector {
        id: 1,
        vector: vec![0.1, 0.2, 0.3],
    }];

    SearchResponse { result }
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new().route("/", web::get().to(index))
                  .route("/search", web::post().to(search)) // Define the route for search endpoint
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}


